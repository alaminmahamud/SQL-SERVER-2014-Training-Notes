-- Combining Predicate
-- AND OR
SELECT * 
FROM Production.Product
WHERE ListPrice > 1000
	AND Color = 'Red'
	OR  Color = 'Black'