-- PREDICATE
-- Predicates are statements about objects that return a boolean result:
-- true
-- false
-- unknown (=NULL)

-- In Computer code you typically find predicates in places where a  yes/no type of decissions has to be taken 

-- For Firebird SQL that means in 
-- ** HAVING
-- ** WHERE
-- ** CHECK
-- ** CASE
-- ** WHEN
-- ** IF
-- ** WHILE