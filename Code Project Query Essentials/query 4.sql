SELECT
	ProductID AS ID,
	Name      AS ProductName,
	ProductNumber AS ProductNumber,
	Color
FROM Production.Product
	