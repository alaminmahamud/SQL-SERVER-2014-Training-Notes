-- NULL

-- this is where stuff gets tricky
-- NULL is actually not a value
-- it is an indication that the attribute of this tuple(or column of this row) 
-- has no  value
-- compare NULL (or no value) to 0
-- SQL SERVER simply treats NULL as not equal to anything else.
-- NULL is not even equal to NULL