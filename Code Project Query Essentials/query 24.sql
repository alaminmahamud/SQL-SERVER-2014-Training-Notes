-- we can use LIKE to find certain words or character in a string,
-- the following query returns all products that are locks(at least according to their names)

SELECT *
FROM Production.Product
WHERE Name LIKE '%Lock%'