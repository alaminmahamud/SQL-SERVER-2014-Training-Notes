-- NOT Takes precedence over AND OR so in the following example the results will contain only red products that are not Expensive than $1000 and all black products

SELECT * 
FROM Production.Product
WHERE NOT ListPrice > 1000
	  AND Color = 'Red'
	  OR  Color = 'Black'
	  