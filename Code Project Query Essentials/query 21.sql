-- Now Suppose you want all products except red and black products that are more expensive than $1000 

-- you can use nested parenthesis and the NOT keyword to simply accomplish the task

SELECT * 
FROM Production.Product
WHERE NOT (ListPrice>1000
	  AND Color = 'Red'
	  OR  Color = 'Black')