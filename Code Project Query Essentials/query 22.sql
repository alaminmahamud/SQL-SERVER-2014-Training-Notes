-- Consider the following query that returns all products starting with a 'B'

SELECT * 
FROM Production.Product
WHERE Name Like 'B%'