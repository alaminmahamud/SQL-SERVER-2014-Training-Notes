-- Consider the following query that returns all products ending with a 'B'

SELECT * 
FROM Production.Product
WHERE Name Like '%B'